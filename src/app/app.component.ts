import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { Category } from './categories/category.model';
import { CategoryService } from './categories/category.service';
import { PostService } from './posts/post.service';
import { Post } from './posts/post.model';


@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

	constructor(private categoryService: CategoryService, private postService: PostService) { }

	ngOnInit() {
		//Fetch the categories once
		this.categoryService.fetchCategories();		
	}
}
