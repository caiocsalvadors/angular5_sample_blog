import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { Post } from './../post.model';
import { PostService } from './../post.service';

@Component({
	selector: 'app-post-detail',
	templateUrl: './post-detail.component.html',
	styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent implements OnInit {

	post: Post;
	id: number;
	subscription: Subscription;

	constructor(
		private postService: PostService,
		private route: ActivatedRoute,
		private router: Router) { }

	ngOnInit() {
		//Subscribing to route changes
		this.route.params.subscribe(
			(params: Params) => {
				this.id = +params['id'];
				//Getting the post
				this.subscription = this.postService.getPost(this.id).subscribe(
					(posts: Post[]) => {
						let selectedPost = posts.filter(x => x['id'] == this.id);		
						//Set the data as a Post Object				
						this.post = new Post(
							selectedPost[0]["id"],
							selectedPost[0]["category_id"],
							selectedPost[0]["title"],
							selectedPost[0]["description"],
							selectedPost[0]["image_url"],
							selectedPost[0]["is_featured"],
							selectedPost[0]["is_active"],
						);
					}
				);
			}
		)
	}
}
