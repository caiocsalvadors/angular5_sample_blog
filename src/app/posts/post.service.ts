import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/map'

import { Post } from './post.model';
import { CategoryService } from './../categories/category.service';
import { Category } from './../categories/category.model';

@Injectable()
export class PostService {

	private posts: Post[];
	private filteredPosts: Post[];
	postsChanged = new Subject<Post[]>();
	subscription: Subscription;
	
	constructor(private http: HttpClient, private categoryService: CategoryService) { 
	}

	//Get posts
	getPosts(category: number) {
		//Make sure if the categories were loaded
		if(this.categoryService.categoriesLoaded) {
			this.http.get<Post[]>('../../assets/json/posts.json').subscribe(posts => {				
				this.setPosts(this.createObjects(posts), category);
			});
		}
		else{
			this.subscription = this.categoryService.categoriesChanged.subscribe(
				(data) => {
				this.http.get<Post[]>('../../assets/json/posts.json').subscribe(posts => {
					this.setPosts(this.createObjects(posts), category);
				});
			});
		}
	}

	//Create Objects Post
	createObjects(posts){
		let newArray = [];
		for (let post of posts) {
			let newPost = new Post(
				post["id"],
				post["category_id"],
				post["title"],
				post["description"],
				post["image_url"],
				post["is_featured"],
				post["is_active"],
			);
			newArray.push(newPost);
		}
		return newArray;
	}

	//Sets the posts inside the array
	setPosts(posts: Post[], category) {	
		this.posts = posts;
		//If the category is not set get the featured posts
		if (category === 0) {
			this.getFeaturedPosts();
		}
		else {
			this.filterPosts(category);
		}
	}

	//Get the featured posts
	getFeaturedPosts(){
		this.filteredPosts = [];
		for (let post of this.posts) {
			if (post.is_featured && post.is_active) {
				this.filteredPosts.push(post);
			}
		}
		//Using the subject to send a copy of the array
		this.postsChanged.next(this.filteredPosts.slice());
	}

	//Get posts by category id
	filterPosts(category_id: number){	
		this.filteredPosts = [];
		//Checking if the category is set
		if (category_id === 0) {
			for (let post of this.posts) {
				if (post.is_featured && post.is_active) {
					this.filteredPosts.push(post);
				}
			}
		}
		//Get posts for a specific category
		else {
			for (let post of this.posts) {				
				if (post.category_id == category_id && post.is_active) {
					this.filteredPosts.push(post);
				}
			}
		}
		//Adding child categories posts
		this.getChildrensPosts(category_id);	
		//Using the subject to send a copy of the array
		this.postsChanged.next(this.filteredPosts);
	}

	//Adding child categories posts
	getChildrensPosts(category_id: number){	
		let parentCategories = this.categoryService.getCategories().filter(x => x.parent_category_id == category_id);
		for (let parent of parentCategories){
			for (let post of this.posts) {
				if (post.category_id == parent.id && post.is_active) {
					this.filteredPosts.push(post);
				}
			}
		}
	}
	//Get post by id
	getPost(id: number) {
		return this.http.get('../../assets/json/posts.json');
	}
}
