import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostDetailComponent } from './post-detail/post-detail.component';
import { PostsComponent } from './posts.component';

const postsRoutes: Routes = [
    { path: ':id', component: PostDetailComponent },
]

@NgModule({
  imports: [RouterModule.forChild(postsRoutes)],
  exports: [RouterModule]
})
export class PostRoutingModule { }
