import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';

import { Post } from './../post.model';
import { PostService } from './../post.service';
import { CategoryService } from './../../categories/category.service';

@Component({
	selector: 'app-post-list',
	templateUrl: './post-list.component.html',
	styleUrls: ['./post-list.component.scss'],
})
export class PostListComponent implements OnInit, OnDestroy {

	posts: Post[];
	subscription: Subscription;
	sub_route: Subscription;
	categoryTitle: string = "Featured posts";

	constructor(
		private postService: PostService, 
		private categoryService:CategoryService,
		private route: ActivatedRoute) { }

	ngOnInit() {
		//Subscribing to route changes
		this.sub_route = this.route.params.subscribe(params => {	
			if (params['id']) {
				this.initData(+params['id']);				
			} else {
				this.initData(null);				
			}
		});
	}
	//Getting the inital data
	initData(params_id: number) {
		//Checking if the posts were loaded
		this.subscription = this.postService.postsChanged.subscribe(
			(posts: Post[]) => {
				this.categoryTitle = this.categoryService.getCategoryTitle(params_id);
				this.posts = posts;								
			}
		)
		//Getting the posts by category
		if (params_id) {
			this.postService.getPosts(params_id);
		}
		//Getting the featured posts
		else {
			this.postService.getPosts(0);
		}
	}
	
	ngOnDestroy() {	
		this.subscription.unsubscribe();
	}
}
