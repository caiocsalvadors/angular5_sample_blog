import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostsComponent } from './posts.component';
import { PostListComponent } from './post-list/post-list.component';
import { PostItemComponent } from './post-list/post-item/post-item.component';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { PostRoutingModule } from './post-routing.module';

@NgModule({
	imports: [
		CommonModule,
		PostRoutingModule
	],
	declarations: [
		PostsComponent,
		PostListComponent,
		PostItemComponent,
		PostDetailComponent
	],
	exports: [
		PostsComponent,
	]
})
export class PostsModule { }
