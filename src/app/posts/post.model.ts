export class Post {
	constructor(
		public id: number,
		public category_id: number,
		public title: string,
		public description: string,
		public image_url: string,
		public is_featured: boolean,
		public is_active: boolean) {
	}
}