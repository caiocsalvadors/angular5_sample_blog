import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map'

import { Category } from './category.model';

@Injectable()
export class CategoryService {
	
	private categories: Category[];
	categoriesChanged = new Subject<Category[]>();	
	categoriesLoaded: boolean = false;

	constructor(private http: HttpClient) { }

	//Fetch data from the file
	fetchCategories(){
		this.categories = [];
		this.http.get<Category[]>('../../assets/json/categories.json')
		.map(
			(categories) =>{
				let newArray: Category[] = [];
				for(let category of categories){
					let newCategory = new Category(
						category["id"],
						category["title"],
						category["parent_category_id"],
						category["is_active"],
					);
					newArray.push(newCategory);
				}
				return newArray;
			}
		)
		.subscribe((categories: Category[]) => {
			this.categoriesLoaded = true;
			this.setCategories(categories);
		});
	}	

	//Get categories
	getCategories() {
		return this.categories.slice();
	}

	//Set the active categories and sort the array correctly
	setCategories(categories: Category[]) {
		for (let category of categories) {
			if (category.is_active) {
				this.categories.push(category);				
			}
		}
		this.sortArray(this.categories);
		this.categoriesChanged.next(this.categories.slice());
	}

	//Get selected title
	getCategoryTitle(id: number) {
		if (id !== null){
			let category = this.categories.filter(x => x.id == id);
			return category[0].title;
		}
		else{
			return "Featured Posts";
		}
	}

	//Sort the array finding parent cagegories and it childrens
	sortArray(categories: Category[]) {
		const oldArray = categories;
		let newArray = [];
		for (let category of oldArray) {
			if (category.parent_category_id != null) {
				const parentIndex = newArray.findIndex(x => x.id == category.parent_category_id);
				if (parentIndex != -1){
					newArray.splice(parentIndex+1, 0, category);
				}
				else{
					newArray.push(category);
				}		
			}
			else{
				newArray.push(category);
			}
		}
		this.categories = newArray;
	}
}