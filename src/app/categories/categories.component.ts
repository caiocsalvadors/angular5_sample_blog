import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { Category } from './category.model';
import { CategoryService } from './category.service';

@Component({
	selector: 'app-categories',
	templateUrl: './categories.component.html',
	styleUrls: ['./categories.component.scss'],
})
export class CategoriesComponent implements OnInit, OnDestroy {
	
	categories: Category[];
	subscription: Subscription;	

	constructor(private categoryService: CategoryService) { }

	ngOnInit() {
		//Wait the categories to be loaded
		this.subscription = this.categoryService.categoriesChanged.subscribe(
			(categories: Category[]) => {				
				this.categories = categories;
			}
		);		
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

}
