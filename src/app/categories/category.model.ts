export class Category {
    constructor(
        public id: number, 
        public title: string, 
        public parent_category_id: number, 
        public is_active: boolean){
    }
}