import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule} from '@angular/router';

import { PostsComponent } from './../posts/posts.component';
import { CategoriesComponent } from './categories.component';

@NgModule({
	imports: [
		CommonModule,
		RouterModule
	],
	declarations: [
		CategoriesComponent,
	],
	exports: [
		CategoriesComponent
	]
})
export class CategoriesModule { }
