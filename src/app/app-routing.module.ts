
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { PostsComponent } from './posts/posts.component';
import { PostListComponent } from './posts/post-list/post-list.component';

const appRoutes: Routes = [
    { path: '', component: PostsComponent },
    { path: 'category/:id', component: PostListComponent },
    //Lazy loading
    { path: 'post', loadChildren: './posts/posts.module#PostsModule' },
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules })],
    exports: [RouterModule]
})
export class AppRoutingModule {

}