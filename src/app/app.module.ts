import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { CategoriesModule } from './categories/categories.module';
import { PostsModule } from './posts/posts.module';
import { HeaderComponent } from './header/header.component';
import { CategoryService } from './categories/category.service';
import { PostService } from './posts/post.service';


@NgModule({
	declarations: [
		AppComponent,
		HeaderComponent,
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		CategoriesModule,
		PostsModule,
		AppRoutingModule,
	],
	providers: [
		CategoryService,
		PostService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
